package game.util;

import game.Game;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Arrays;
import java.util.Objects;

public class Resource
{
	private File resourceDir;

	public Resource()
	{
		try
		{
			URL classUrl = getClass().getProtectionDomain().getCodeSource().getLocation();
			if (classUrl == null) throw new IOException();

			File projectDir = new File(classUrl.toURI()).getParentFile();

			if (projectDir == null) throw new IOException();

			File[] files = projectDir.listFiles((f) -> f.getName().equals("resources") && f.isDirectory());
			if (files == null || files.length != 1) throw new IOException();

			resourceDir = files[0];

			if (!resourceDir.isDirectory()) throw new IOException();
		}
		catch (Exception e)
		{
			Game.getPrinter().printError("Couldn't locate resource folder");
			e.printStackTrace();
			System.exit(1);
		}
	}

	public String getAbsolutePath(String relativePath)
	{
		return resourceDir.getPath() + relativePath;
	}

	public File getFile(String path)
	{
		return new File(getAbsolutePath(path));
	}

	public File create(File file)
	{
		if (file.exists()) return file;

		File parent = file.getParentFile();

		try
		{
			if (!parent.exists() && !parent.mkdirs())
			{
				throw new IllegalStateException("Couldn't create dir: " + parent.getPath());
			}

			if (!file.createNewFile())
			{
				throw new IllegalStateException("Couldn't create file: " + file.getPath());
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

		return file;
	}
}
