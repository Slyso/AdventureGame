package game.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

public class FileReader
{
	private BufferedReader reader;
	private static final String DELIMITER = ": ";

	public FileReader(File file)
	{
		try
		{
			reader = new BufferedReader(new java.io.FileReader(file));
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
	}

	public Map<String, String> getContent()
	{
		Map<String, String> map = new HashMap<>();

		reader.lines().forEach(line ->
		{
			if (line.split(DELIMITER).length == 2)
			{
				map.put(line.split(DELIMITER)[0], line.split(DELIMITER)[1]);
			}
		});

		return map;
	}
}
