/*
 * Copyright (C) 2018 Thomas Zahner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package game.graphics.gui;

import game.Game;
import game.entity.item.ItemStack;
import game.entity.item.ItemType;
import game.graphics.Screen;
import game.graphics.Sprite;

public class InventorySlot
{
	private int xPos, yPos;
	public static final int SIZE = 16;

	private ItemStack stack;

	public InventorySlot(ItemType itemType, int xPos, int yPos)
	{
		stack = new ItemStack(itemType);
		this.xPos = xPos;
		this.yPos = yPos;
	}

	public ItemStack getStack()
	{
		return stack;
	}

	public int getXPos()
	{
		return xPos;
	}

	public int getYPos()
	{
		return yPos;
	}

	public boolean isClicked(int x, int y)
	{
		return x >= xPos * Game.SCALE && y >= yPos * Game.SCALE && x <= (xPos + SIZE) * Game.SCALE && y <= (yPos + SIZE) * Game.SCALE;
	}
}
