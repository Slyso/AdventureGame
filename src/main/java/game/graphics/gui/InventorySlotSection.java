/*
 * Copyright (C) 2018 Thomas Zahner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package game.graphics.gui;

import game.entity.item.Item;
import game.entity.item.ItemType;
import game.graphics.Screen;
import game.graphics.Sprite;

import java.util.ArrayList;
import java.util.List;

public class InventorySlotSection
{
	private List<InventorySlot> slots = new ArrayList<>();

	public InventorySlotSection(ItemType itemType, int width, int height, int xStart, int yStart, int xSpacing, int ySpacing)
	{
		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				slots.add(new InventorySlot(itemType, xStart + x * (xSpacing + InventorySlot.SIZE), yStart + y * (ySpacing + InventorySlot.SIZE)));
			}
		}
	}

	public List<InventorySlot> getSlots()
	{
		return slots;
	}

	public InventorySlot getClickedSlot(int mouseX, int mouseY)
	{
		return slots.stream().filter(slot -> slot.isClicked(mouseX, mouseY)).findFirst().orElse(null);
	}

	public void render(Screen screen)
	{
		slots.forEach(slot -> slot.getStack().render(screen, slot.getXPos(), slot.getYPos()));
	}
}
