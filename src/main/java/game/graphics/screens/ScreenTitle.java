/*******************************************************************************
 * Copyright (C) 2018 Thomas Zahner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package game.graphics.screens;

import game.Game;
import game.graphics.Screen;
import game.graphics.screens.selection.SelectionMainMenu;
import game.graphics.screens.selection.SelectionScreen;
import game.graphics.Sprite;
import game.input.Keyboard;
import game.level.tile.Tile;
import game.settings.Settings;

public class ScreenTitle
{
	private static float xOffset = 0;
	private static float yOffset = 0;
	private static float xVelocity = 0.5F;
	private static float yVelocity = 0.5F;

	private static SelectionScreen currentSelectionScreen = new SelectionMainMenu(null);

	public static void tick(Keyboard input)
	{
		tickLevel();
		tickSelectionScreen(input);
	}

	public static void render(Screen screen)
	{
		if (Game.getLevel() != null) Game.getLevel().render((int) xOffset, (int) yOffset, screen);

		if (Settings.maxGraphicsQuality.getValue())
		{
			screen.blur();
			screen.blur();
		}
		else screen.blur();

		String[] selections = currentSelectionScreen.getSelections();

		for (int i = 0; i < selections.length; i++)
		{
			boolean isSelected = currentSelectionScreen.getCurrentSelection() == i;
			Sprite.writeText(selections[i], screen, screen.width / 2, Game.height / 2 + (i * 50 - 70), isSelected ? 0x45C95E : 0x000000);
		}
	}

	public static void setSelectionScreen(SelectionScreen selectionScreen)
	{
		currentSelectionScreen = selectionScreen;
	}

	public static SelectionScreen getCurrentSelectionScreen()
	{
		return currentSelectionScreen;
	}

	private static void tickLevel()
	{
		if (Game.getLevel() == null) Game.loadTitleScreenLevel();
		Game.getLevel().tick();

		xOffset += xVelocity;
		yOffset += yVelocity;

		if (yOffset > Game.getLevel().getLevelHeight() * Tile.DEFAULT_TILE_SIZE - Game.height) yVelocity = -yVelocity;
		else if (yOffset < 0) yVelocity = -yVelocity;

		if (xOffset > Game.getLevel().getLevelWidth() * Tile.DEFAULT_TILE_SIZE - Game.width) xVelocity = -xVelocity;
		else if (xOffset < 0) xVelocity = -xVelocity;
	}

	private static void tickSelectionScreen(Keyboard input)
	{
		input.tick();
		currentSelectionScreen.tick(input);
	}
}
