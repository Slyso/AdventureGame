package game.graphics.screens.selection;

import game.graphics.screens.ScreenTitle;
import game.input.Keyboard;

public abstract class SelectionScreen
{
	private int currentSelection = 0;
	private int maxSelection;

	private String[] selections;
	private SelectionScreen parent;

	public SelectionScreen(String[] selections, SelectionScreen parent)
	{
		setSelections(selections);
		this.parent = parent;
	}

	public SelectionScreen(SelectionScreen parent)
	{
		this.parent = parent;
	}

	public abstract void onTriggerSelection();

	public abstract void onIncreaseCurrentSelection();

	public abstract void onDecreaseCurrentSelection();

	public void tick(Keyboard input)
	{
		handleNavigation(input);
		handleModification(input);

		if (input.escapeToggle && getParent() != null) onExit();
	}

	public String[] getSelections()
	{
		return selections;
	}

	public int getCurrentSelection()
	{
		return currentSelection;
	}

	protected void onExit()
	{
		getParent().onResume();
		ScreenTitle.setSelectionScreen(getParent());
	}

	protected void onResume()
	{
	}

	protected void setSelections(String[] selections)
	{
		this.selections = selections;
		maxSelection = selections.length - 1;
	}

	protected void setCurrentSelection(String selection)
	{
		selections[currentSelection] = selection;
	}

	private SelectionScreen getParent()
	{
		return parent;
	}

	private void handleNavigation(Keyboard input)
	{
		if (input.downToggle)
		{
			if (++currentSelection > maxSelection) currentSelection = 0;
		}
		else if (input.upToggle)
		{
			if (--currentSelection < 0) currentSelection = maxSelection;
		}
	}

	private void handleModification(Keyboard input)
	{
		if (input.enterToggle || input.spaceToggle) onTriggerSelection();
		if (input.rightToggle) onIncreaseCurrentSelection();
		else if (input.leftToggle) onDecreaseCurrentSelection();
	}
}
