package game.graphics.screens.selection;

import game.Game;
import game.graphics.screens.ScreenTitle;

public class SelectionOnline extends SelectionScreen
{
	private static final String CONNECT = "Connect to server";
	private static final String NEW_SERVER = "Start new server";

	public SelectionOnline(SelectionScreen parent)
	{
		super(new String[]{CONNECT, NEW_SERVER}, parent);
	}

	@Override
	public void onTriggerSelection()
	{
		switch (getSelections()[getCurrentSelection()])
		{
			case CONNECT:
				ScreenTitle.setSelectionScreen(new SelectionServer(this));
				break;

			case NEW_SERVER:
				Game.unloadLevel();
				Game.startServer();
				break;
		}
	}

	@Override
	public void onIncreaseCurrentSelection()
	{
	}

	@Override
	public void onDecreaseCurrentSelection()
	{
	}
}
