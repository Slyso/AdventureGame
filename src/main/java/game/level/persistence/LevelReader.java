package game.level.persistence;

import game.Game;
import game.level.Level;
import game.util.FileReader;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class LevelReader
{
	private final File levelDirectory;

	public LevelReader()
	{
		levelDirectory = Game.getResource().getFile("/persistence");
	}

	public List<String> getLevelNames()
	{
		List<String> names = new ArrayList<>();
		List<File> levelFiles = getLevelFiles();

		if (levelFiles == null) return null;

		levelFiles.forEach(f -> names.add(f.getName().replace(".lvl", "")));
		return names;
	}

	public List<Level> readLevels()
	{
		List<Level> levels = new ArrayList<>();
		List<File> levelFiles = getLevelFiles();

		levelFiles.forEach(file -> levels.add(loadLevelFrom(file)));
		return levels;
	}

	public int getNumberOfLevels()
	{
		List<File> levelFiles = getLevelFiles();

		if (levelFiles == null) return 0;
		return levelFiles.size();
	}

	private Level loadLevelFrom(File file)
	{
		FileReader fileReader = new FileReader(file);

		for (Map.Entry<String, String> entry : fileReader.getContent().entrySet())
		{
			System.out.println(entry.getKey());
			System.out.println(entry.getValue());
		}

		//		return new LevelLoader().fromSeed(seed == 0 ? rand.nextLong() : seed).load(key, "GeneratedLevel")
		return null;
	}

	private List<File> getLevelFiles()
	{
		File[] files = levelDirectory.listFiles();
		if (files == null) return null;

		List<File> levelFiles = new ArrayList<>(Arrays.asList(files));
		return levelFiles.stream().filter(f -> f.getPath().endsWith(".lvl")).collect(Collectors.toList());
	}
}
