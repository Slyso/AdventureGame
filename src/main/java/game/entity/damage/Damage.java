/*******************************************************************************
 * Copyright (C) 2018 Thomas Zahner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package game.entity.damage;

import game.Game;
import game.audio.PlaySound;
import game.audio.Sounds;
import game.entity.Entity;
import game.entity.mob.Mob;
import game.entity.spawner.ParticleSpawner;
import game.graphics.Sprite;
import game.network.serialization.Serializable;
import game.network.serialization.SerializationReader;
import game.network.serialization.types.SField;
import game.network.serialization.types.SObject;
import game.network.serialization.types.SObjectType;
import game.network.serialization.types.SString;
import game.util.GameState;

import java.util.UUID;

public class Damage extends Entity implements Serializable
{
	private float damage;
	private Mob mob;

	public Damage(Mob mob, float damage)
	{
		this.damage = damage;
		this.mob = mob;
	}

	@Override
	public void tick()
	{
		if (mob != null)
		{
			if (!(Game.getGameState() == GameState.IngameOnline && !Game.isHostingGame))
			{
				mob.setHealth(mob.getCurrentHealth() - damage);
			}

			setPosition(mob.getX(), mob.getY());
		}

		spawnBlood();
		PlaySound.playSound(Sounds.hurt);
		level.add(new DamageValue(damage, x, y));

		remove();
	}

	private void spawnBlood()
	{
		level.add(new ParticleSpawner(x, y, 0.85F, 0.75F, 100, 20, Sprite.PARTICLE_BLOOD));
	}

	@Override
	public SObject serialize()
	{
		SObject object = new SObject(SObjectType.DAMAGE);

		object.addField(SField.Float("damage", damage));
		object.addField(SField.Integer("x", x));
		object.addField(SField.Integer("y", y));

		object.addString(SString.String("mob", mob.getUUID().toString()));

		return object;
	}

	@Override
	public void deserialize(SObject object)
	{
		SField damage = object.findField("damage");
		SField x = object.findField("x");
		SField y = object.findField("y");

		SString mob = object.findString("mob");

		if (damage == null || x == null || y == null || mob == null) return;

		this.damage = SerializationReader.readFloat(damage.getData(), 0);
		this.x = SerializationReader.readInt(x.getData(), 0);
		this.y = SerializationReader.readInt(y.getData(), 0);

		this.mob = Game.getLevel().getMob(UUID.fromString(mob.getValue()));
	}
}

