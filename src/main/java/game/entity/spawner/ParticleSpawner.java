/*******************************************************************************
 * Copyright (C) 2018 Thomas Zahner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package game.entity.spawner;

import java.util.Random;

import game.entity.Entity;
import game.entity.particle.Particle;
import game.graphics.Sprite;
import game.level.Level;
import game.network.serialization.Serializable;
import game.network.serialization.SerializationReader;
import game.network.serialization.types.SField;
import game.network.serialization.types.SObject;
import game.network.serialization.types.SObjectType;
import game.network.serialization.types.SString;

public class ParticleSpawner extends Entity
{
	private Random rand = new Random();

	private float xSpeed, ySpeed;
	private int life;
	private int amount;
	private Sprite[] sprites;

	@Override
	public void tick()
	{
		if (sprites == null) sprites = Sprite.getParticleSpritesFromPosition(x, y, amount);

		for (int i = 0; i < amount; i++)
		{
			level.add(new Particle(x, y, xSpeed, ySpeed, life, sprites[rand.nextInt(sprites.length)]));
		}

		remove();
	}

	/**
	 * Spawn random sprites from the sprites array.
	 */
	public ParticleSpawner(int x, int y, float xSpeed, float ySpeed, int life, int amount, Sprite[] sprites)
	{
		init(x, y, xSpeed, ySpeed, life, amount, sprites);
	}

	/**
	 * Generate sprites similar to the colour from the background at the position
	 */
	public ParticleSpawner(int x, int y, float xSpeed, float ySpeed, int life, int amount)
	{
		init(x, y, xSpeed, ySpeed, life, amount, null);
	}

	private void init(int x, int y, float xSpeed, float ySpeed, int life, int amount, Sprite[] sprites)
	{
		this.x = x;
		this.y = y;
		this.xSpeed = xSpeed;
		this.ySpeed = ySpeed;

		this.life = life;
		this.amount = amount;
		this.sprites = sprites;
	}

}
