/*******************************************************************************
 * Copyright (C) 2018 Thomas Zahner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package game.entity.mob.player;

import game.network.serialization.types.SObject;
import game.network.serialization.types.SString;

import java.util.UUID;

public class OnlinePlayer extends Player
{
	private String playerName;

	public OnlinePlayer(int xPos, int yPos, String IPAddress, String playerName)
	{
		super(xPos, yPos, null, IPAddress, null);
		this.playerName = playerName;
	}

	public OnlinePlayer(int xPos, int yPos, UUID uuid, String playerName)
	{
		super(xPos, yPos, null, null, uuid);
		this.playerName = playerName;
	}

	public String getPlayerName()
	{
		return playerName;
	}

	@Override
	public SObject serialize()
	{
		SObject object = super.serialize();
		object.addString(SString.String("name", playerName));

		return object;
	}

	@Override
	public void deserialize(SObject object)
	{
		super.deserialize(object);

		if (object == null) return;

		SString name = object.findString("name");
		if (name != null) playerName = name.getValue();
	}
}
