/*******************************************************************************
 * Copyright (C) 2018 Thomas Zahner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package game.entity.mob.ability;

import game.Game;
import game.entity.mob.player.Player;
import game.entity.projectile.ProjectileBoomerang;
import game.entity.projectile.ProjectileBullet;
import game.entity.projectile.ProjectileGrenade;
import game.entity.projectile.ProjectileGuardian;
import game.entity.projectile.Projectiles;
import game.input.Mouse;
import game.level.Level;
import game.network.serialization.SerializationReader;
import game.network.serialization.types.SField;
import game.network.serialization.types.SObject;
import game.util.GameState;

public class AbilityShooting extends Ability
{
	private Projectiles projectile;
	private double angle;

	private final boolean clientPlayer;

	public AbilityShooting(Player player, Projectiles projectile, int cooldown)
	{
		super(player, cooldown);
		this.projectile = projectile;

		Level level = Game.getLevel();
		clientPlayer = level == null || level.getClientPlayer().equals(player);
	}

	@Override
	protected void onEnable()
	{
		if (clientPlayer)
		{
			int deltaX = Mouse.getX() / Game.SCALE - Game.getLevel().getClientPlayer().getX() + Game.getScreen().getXOffset();
			int deltaY = Mouse.getY() / Game.SCALE - Game.getLevel().getClientPlayer().getY() + Game.getScreen().getYOffset();

			angle = Math.atan2(deltaY, deltaX); //Atan = tan^-1, difference to atan: doesn't crash when dividing by 0, = atan(deltaY / deltaX)
		}

		if (Game.getGameState() == GameState.IngameOffline || Game.isHostingGame)
		{
			switch (projectile)
			{
				case ProjectileBoomerang:
					player.shoot(new ProjectileBoomerang(player.getX(), player.getY(), angle, player, null));
					break;
				case ProjectileBullet:
					player.shoot(new ProjectileBullet(player.getX(), player.getY(), angle, player, null));
					break;
				case ProjectileGuardian:
					player.shoot(new ProjectileGuardian(player.getX(), player.getY(), angle, player, null));
					break;
				case ProjectileGrenade:
					player.shoot(new ProjectileGrenade(player.getX(), player.getY(), angle, player, null));
					break;
			}
		}
	}

	public SObject serialize(SObject object)
	{
		object.addField(SField.Float("angle", (float) angle));
		return object;
	}

	public void deserialize(SObject object)
	{
		SField angle = object.findField("angle");
		if (angle != null) this.angle = SerializationReader.readFloat(angle.getData(), 0);
	}
}
