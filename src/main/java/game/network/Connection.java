/*******************************************************************************
 * Copyright (C) 2018 Thomas Zahner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package game.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.UnknownHostException;

import game.Game;
import game.network.ingame.GetSendDataAsClient;

public class Connection implements Runnable
{
	private Thread connectionThread;
	private volatile boolean running = false;

	private DatagramSocket hostSocket, clientSocket;
	private NetworkPackage networkPackage;

	private static final int PACKET_SIZE = 4096;
	private int port = 22592;

	private String ip;
	private final boolean isClient;
	public boolean connectionEstablished = false;

	public Connection(String ip, boolean asHost)
	{
		this.ip = ip;
		isClient = !asHost;
	}

	public void connect()
	{
		if (isClient) prepareClient();
		else
		{
			if (!initHostSocket())
			{
				Game.abortMultiplayer("Socket initialization filed");
				return;
			}

			if (!fetchHostIp())
			{
				Game.abortMultiplayer("Couldn't fetch ip");
				return;
			}

			connectionEstablished = true;
		}

		networkPackage = new NetworkPackage(isClient);
		tick(); //Peekaboo tick
	}

	public void tick()
	{
		byte[] receiveData;
		if (isClient)
		{
			try
			{
				receiveData = new byte[PACKET_SIZE];
				InetAddress IPAddress = InetAddress.getByName(ip);

				//Send data
				byte[] data = networkPackage.getSendData(IPAddress.getHostName());
				DatagramPacket sendPacket = new DatagramPacket(data, data.length, IPAddress, port);
				clientSocket.send(sendPacket);

				//Receive data
				DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
				clientSocket.receive(receivePacket);
				networkPackage.receiveDataAsClient(receiveData);
				connectionEstablished = true;
			}
			catch (IOException e)
			{
				connectionEstablished = false;

				if (e instanceof UnknownHostException) Game.abortMultiplayer("Unknown host!");
				else if (e instanceof SocketTimeoutException) Game.abortMultiplayer("Connection timeout!");
				else
				{
					Game.abortMultiplayer("Connection error!");
					e.printStackTrace();
				}
			}
		}
		else
		{
			try
			{
				receiveData = new byte[PACKET_SIZE];
				DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);

				try
				{
					hostSocket.receive(receivePacket); //Waits for client to connect
				}
				catch (IOException e)
				{
					if (!(e instanceof SocketTimeoutException)) e.printStackTrace();
					return;
				}

				InetAddress IPAddress = receivePacket.getAddress();

				//Receive data
				int port = receivePacket.getPort();
				networkPackage.receiveDataAsHost(receiveData, IPAddress.getHostName());

				//Send data
				byte[] data = networkPackage.getSendData(IPAddress.getHostName());
				DatagramPacket sendPacket = new DatagramPacket(data, data.length, IPAddress, port);
				hostSocket.send(sendPacket);
			}
			catch (IOException e)
			{
				Game.getPrinter().printError("Couldn't send packet!");
				e.printStackTrace();
			}
		}
	}

	public void close()
	{
		if (connectionThread != null)
		{
			try
			{
				running = false;
				connectionThread.join();
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}

		if (isClient && connectionEstablished)
		{
			GetSendDataAsClient.disconnect();
			tick();
		}

		if (clientSocket != null) clientSocket.close();
		if (hostSocket != null) hostSocket.close();

		Game.getPrinter().printInfo("Connection closed");
	}

	public void run()
	{
		if (isClient)
		{
			while (running)
			{
				tick();
			}
		}
		else
		{
			//Runs at the same TPS as the game, but less precise
			long lastTime = System.nanoTime();
			final double nsPerTick = 1_000_000_000.0 / Game.TPS;
			double delta = 0;

			while (running)
			{
				long now = System.nanoTime();
				delta += (now - lastTime) / nsPerTick;
				lastTime = now;

				if (delta >= 1)
				{
					tick();
					delta = 0; //Just reset, ignore any overflow
				}
			}
		}

	}

	private boolean initHostSocket()
	{
		try
		{
			hostSocket = new DatagramSocket(port);
			hostSocket.setSoTimeout(1000);
			return true;
		}
		catch (SocketException e)
		{
			return false;
		}
	}

	private boolean fetchHostIp()
	{
		ip = getPublicIP();
		String localIp;

		try
		{
			localIp = InetAddress.getLocalHost().getHostAddress();
		}
		catch (UnknownHostException e)
		{
			return false;
		}

		if (ip == null)
		{
			Game.getPrinter().printImportantInfo("You are not connected to the internet!");
			Game.getPrinter().printImportantInfo("Started server, local IP: " + localIp);
		}
		else
		{
			Game.getPrinter().printImportantInfo("Started server, IP: " + ip + " or " + localIp + " (local)");
		}

		return true;
	}

	private void prepareClient()
	{
		try
		{
			clientSocket = new DatagramSocket(null);
			clientSocket.setSoTimeout(2000);
		}
		catch (SocketException e)
		{
			Game.getPrinter().printError(e.getMessage());
		}
	}

	private String getPublicIP()
	{
		try
		{
			URL whatIsMyIp;
			whatIsMyIp = new URL("http://checkip.amazonaws.com");
			BufferedReader in = new BufferedReader(new InputStreamReader(whatIsMyIp.openStream()));
			return in.readLine();
		}
		catch (IOException e)
		{
			return null;
		}
	}

	public synchronized void startThread()
	{
		running = true;
		connectionThread = new Thread(this, "Connection");
		connectionThread.start();
	}
}
