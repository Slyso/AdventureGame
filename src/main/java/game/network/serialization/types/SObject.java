/*******************************************************************************
 * Copyright (C) 2018 Thomas Zahner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package game.network.serialization.types;

import game.network.serialization.SerializationReader;

import static game.network.serialization.SerializationWriter.writeBytes;

import java.util.ArrayList;
import java.util.List;

public class SObject
{
	private static final byte CONTAINER_TYPE = (byte) ContainerType.OBJECT.ordinal();
	private SObjectType type;

	private List<SField> fields = new ArrayList<>();
	private List<SArray> arrays = new ArrayList<>();
	private List<SString> strings = new ArrayList<>();

	private SObject()
	{
	}

	public SObject(SObjectType type)
	{
		setType(type);
	}

	public void setType(SObjectType type)
	{
		this.type = type;
	}

	public int getBytes(byte[] destination, int pointer)
	{
		pointer = writeBytes(destination, pointer, CONTAINER_TYPE);
		pointer = writeBytes(destination, pointer, (byte) type.ordinal());

		pointer = writeBytes(destination, pointer, fields.size());

		for (SField field : fields)
		{
			pointer = field.getBytes(destination, pointer);
		}

		pointer = writeBytes(destination, pointer, arrays.size());

		for (SArray array : arrays)
		{
			pointer = array.getBytes(destination, pointer);
		}

		pointer = writeBytes(destination, pointer, strings.size());

		for (SString string : strings)
		{
			pointer = string.getBytes(destination, pointer);
		}
		return pointer;
	}

	public void addField(SField field)
	{
		if (field != null) fields.add(field);
	}

	public void addArray(SArray array)
	{
		if (array != null) arrays.add(array);
	}

	public void addString(SString string)
	{
		if (string != null) strings.add(string);
	}

	public void removeField(String name)
	{
		fields.remove(findField(name));
	}

	public void removeString(String name)
	{
		strings.remove(findString(name));
	}

	public SField findField(String name)
	{
		return fields.stream().filter(f -> f.getName().equals(name)).findFirst().orElse(null);
	}

	public SString findString(String name)
	{
		return strings.stream().filter(s -> s.getName().equals(name)).findFirst().orElse(null);
	}

	public int getSize()
	{
		int size = 1 + 1 + 3 * 4;

		size += fields.stream().mapToInt(SField::getSize).sum();
		size += arrays.stream().mapToInt(SArray::getSize).sum();
		size += strings.stream().mapToInt(SString::getSize).sum();

		return size;
	}

	public SObjectType getType()
	{
		return type;
	}

	public static SObject deserialize(byte[] data, int pointer)
	{
		byte containerType = SerializationReader.readByte(data, pointer++);
		if (containerType != ContainerType.OBJECT.ordinal()) return null;

		SObject object = new SObject();

		byte typeIndex = SerializationReader.readByte(data, pointer++);
		if (typeIndex < 0 || typeIndex >= SObjectType.values().length) return null;
		object.type = SObjectType.values()[typeIndex];

		int fieldCount = SerializationReader.readInt(data, pointer);
		pointer += 4;
		for (int i = 0; i < fieldCount; i++)
		{
			SField field = SField.deserialize(data, pointer);
			if (field == null) return null;
			object.addField(field);
			pointer += field.getSize();
		}

		int arrayCount = SerializationReader.readInt(data, pointer);
		pointer += 4;
		for (int i = 0; i < arrayCount; i++)
		{
			//TODO
		}

		int stringCount = SerializationReader.readInt(data, pointer);
		pointer += 4;
		for (int i = 0; i < stringCount; i++)
		{
			SString string = SString.deserialize(data, pointer);
			if (string == null) return null;
			object.addString(string);
			pointer += string.getSize();
		}

		return object;
	}
}
