/*******************************************************************************
 * Copyright (C) 2018 Thomas Zahner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package game.network.serialization.types;

import game.network.serialization.SerializationReader;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static game.network.serialization.SerializationWriter.writeBytes;

public class SClass
{
	private static final byte VERSION = 3;

	private static final byte CONTAINER_TYPE = (byte) ContainerType.CLASS.ordinal();
	private short nameLength;
	private byte[] name;

	private List<SObject> objects = new ArrayList<>();

	private SClass()
	{
	}

	public SClass(String name)
	{
		setName(name);
	}

	public void setName(String name)
	{
		nameLength = (short) name.length();
		this.name = name.getBytes();
	}

	public void addObject(SObject sObject)
	{
		if (sObject != null) objects.add(sObject);
	}

	public void getBytes(byte[] destination, int pointer)
	{
		pointer = writeBytes(destination, pointer, CONTAINER_TYPE);
		pointer = writeBytes(destination, pointer, VERSION);
		pointer = writeBytes(destination, pointer, nameLength);
		pointer = writeBytes(destination, pointer, name);

		pointer = writeBytes(destination, pointer, objects.size());

		for (SObject object : objects)
		{
			pointer = object.getBytes(destination, pointer);
		}
	}

	public static SClass deserialize(byte[] data, int pointer)
	{
		byte containerType = SerializationReader.readByte(data, pointer++);
		if (containerType != CONTAINER_TYPE) return null;

		byte version = SerializationReader.readByte(data, pointer++);
		if (version != VERSION) return null;

		SClass sClass = new SClass();

		sClass.nameLength = SerializationReader.readShort(data, pointer);
		pointer += 2;

		sClass.name = SerializationReader.readString(data, pointer, sClass.nameLength).getBytes();
		pointer += sClass.nameLength;

		int objectCount = SerializationReader.readInt(data, pointer);
		pointer += 4;

		for (int i = 0; i < objectCount; i++)
		{
			SObject object = SObject.deserialize(data, pointer);
			if (object == null)
			{
				return null;
			}

			sClass.addObject(object);
			pointer += object.getSize();
		}

		return sClass;
	}

	public SObject getObject(SObjectType type)
	{
		return objects.stream().filter(object -> object.getType().equals(type)).findFirst().orElse(null);
	}

	public List<SObject> getObjects(SObjectType type)
	{
		return objects.stream().filter(object -> object.getType().equals(type)).collect(Collectors.toList());
	}

	public String getName()
	{
		return SerializationReader.readString(name, 0, nameLength);
	}

	public int getSize()
	{
		int size = 1 + 1 + 2 + nameLength + 4;

		for (SObject object : objects)
		{
			size += object.getSize();
		}

		return size;
	}
}
