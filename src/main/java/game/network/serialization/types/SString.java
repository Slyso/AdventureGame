/*******************************************************************************
 * Copyright (C) 2018 Thomas Zahner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package game.network.serialization.types;

import game.network.serialization.SerializationReader;
import game.network.serialization.SerializationWriter;

public class SString
{
	private static final byte CONTAINER_TYPE = (byte) ContainerType.STRING.ordinal();

	private String name;
	private String value;

	private SString()
	{
	}

	public void setValue(String value)
	{
		this.value = value;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public int getBytes(byte[] destination, int pointer)
	{
		pointer = SerializationWriter.writeBytes(destination, pointer, CONTAINER_TYPE);

		pointer = SerializationWriter.writeBytes(destination, pointer, name.getBytes().length);
		pointer = SerializationWriter.writeBytes(destination, pointer, name);
		pointer = SerializationWriter.writeBytes(destination, pointer, value.getBytes().length);
		pointer = SerializationWriter.writeBytes(destination, pointer, value);

		return pointer;
	}

	public int getSize()
	{
		return 1 + 4 + name.getBytes().length + 4 + value.getBytes().length;
	}

	public String getName()
	{
		return name;
	}

	public String getValue()
	{
		return value;
	}

	public static SString String(String name, String value)
	{
		SString string = new SString();
		string.name = name;
		string.value = value;
		return string;
	}

	public static SString deserialize(byte[] data, int pointer)
	{
		byte containerType = SerializationReader.readByte(data, pointer);
		pointer++;
		if (containerType != ContainerType.STRING.ordinal()) return null;

		SString sString = new SString();

		int nameLength = SerializationReader.readInt(data, pointer);
		pointer += 4;
		sString.name = SerializationReader.readString(data, pointer, nameLength);
		pointer += sString.name.getBytes().length;

		int valueLength = SerializationReader.readInt(data, pointer);
		pointer += 4;
		sString.value = SerializationReader.readString(data, pointer, valueLength);

		return sString;
	}
}
