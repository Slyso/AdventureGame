/*******************************************************************************
 * Copyright (C) 2018 Thomas Zahner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package game.network;

import game.network.ingame.GetSendDataAsClient;
import game.network.ingame.GetSendDataAsHost;
import game.network.ingame.ReceiveDataAsClient;
import game.network.ingame.ReceiveDataAsHost;
import game.network.serialization.types.SClass;

public class NetworkPackage
{
	public static final String CLASS_NAME = "main";
	private boolean isClient;

	public NetworkPackage(boolean isClient)
	{
		this.isClient = isClient;
	}

	public byte[] getSendData(String IPAddress)
	{
		SClass sClass = new SClass(CLASS_NAME);

		if (isClient) return GetSendDataAsClient.getData(sClass);
		else return GetSendDataAsHost.getData(sClass, IPAddress);
	}

	public void receiveDataAsClient(byte[] data)
	{
		ReceiveDataAsClient.receive(data);
	}

	public void receiveDataAsHost(byte[] data, String IPAddressSender)
	{
		ReceiveDataAsHost.receiveData(data, IPAddressSender);
	}
}
