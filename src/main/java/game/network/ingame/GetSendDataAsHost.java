/*******************************************************************************
 * Copyright (C) 2018 Thomas Zahner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package game.network.ingame;

import game.Game;
import game.entity.mob.Mob;
import game.entity.mob.player.OnlinePlayer;
import game.entity.mob.player.Player;
import game.network.Server;
import game.network.serialization.types.*;

import java.util.Collections;
import java.util.List;

public class GetSendDataAsHost
{
	public static byte[] getData(SClass sClass, String IPAddress)
	{
		if (Game.getLevel() == null) return getData(sClass);

		if (Server.isClientBanned(IPAddress) || !Server.isClientOnline(IPAddress))
		{
			return reject(sClass, Server.isClientBanned(IPAddress));
		}

		if (Server.clientShouldLoadLevel(IPAddress))
		{
			SObject level = new SObject(SObjectType.LEVEL);
			level.addField(SField.Long("seed", Game.getLevel().getSeed()));
			level.addString(SString.String("uuid", Game.getLevel().getPlayerByIP(IPAddress).getUUID().toString()));

			sClass.addObject(level);
			Server.setClientHasLoadedLevel(IPAddress, true);
		}

		SerializeEntities.tick(sClass);

		return getData(sClass);
	}

	private static byte[] reject(SClass sClass, boolean isBanned)
	{
		SObject reject = new SObject(SObjectType.REJECT);

		if (isBanned)
		{
			reject.addString(SString.String("reason", "you are banned from this server"));
		}
		else
		{
			reject.addString(SString.String("reason", "join failed"));
		}

		sClass.addObject(reject);

		byte[] data = new byte[sClass.getSize()];
		sClass.getBytes(data, 0);

		return data;
	}

	private static byte[] getData(SClass sClass)
	{
		byte[] data = new byte[sClass.getSize()];
		sClass.getBytes(data, 0);

		return data;
	}
}
