/*******************************************************************************
 * Copyright (C) 2018 Thomas Zahner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package game.network.ingame;

import game.Game;
import game.network.NetworkPackage;
import game.network.serialization.SerializationReader;
import game.network.serialization.types.*;

import java.util.UUID;

public class ReceiveDataAsClient
{
	public static void receive(byte[] data)
	{
		SClass sClass = SClass.deserialize(data, 0);
		if (sClass == null || !sClass.getName().equals(NetworkPackage.CLASS_NAME))
		{
			if (Game.getLevel() == null) Game.abortMultiplayer("Connection failed");
			return;
		}

		if (rejected(sClass)) return;

		if (Game.getLevel() == null) initLevel(sClass);
		else DeserializeEntities.tick(sClass);
	}

	private static void initLevel(SClass sClass)
	{
		SObject sLevel = sClass.getObject(SObjectType.LEVEL);
		if (sLevel == null) return;

		SField seed = sLevel.findField("seed");
		SString uuid = sLevel.findString("uuid");

		if (seed != null && uuid != null)
		{
			Game.loadLevel(SerializationReader.readLong(seed.getData(), 0));
			Game.getLevel().getClientPlayer().setUUID(UUID.fromString(uuid.getValue()));
		}
	}

	private static boolean rejected(SClass sClass)
	{
		SObject rejection = sClass.getObject(SObjectType.REJECT);
		if (rejection == null) return false;

		SString reason = rejection.findString("reason");

		String message = "You got kicked from the server!";
		if (reason != null) message += (" Reason: " + reason.getValue());

		Game.getConnection().connectionEstablished = false;
		Game.abortMultiplayer(message);

		return true;
	}
}
