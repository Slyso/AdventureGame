package game.settings;

public class SettingRange extends Setting<Integer>
{
	private int minValue, maxValue;

	public SettingRange(String name, int minValue, int maxValue, int standardValue)
	{
		super(name, standardValue);
		this.minValue = minValue;
		this.maxValue = maxValue;
	}

	public void setValue(Integer value)
	{
		this.value = value;
		if (this.value < minValue) this.value = maxValue;
		if (this.value > maxValue) this.value = minValue;
	}

	public void increaseValue()
	{
		setValue(value + 1);
	}

	public void decreaseValue()
	{
		setValue(value - 1);
	}
}