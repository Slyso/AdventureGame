package game.settings;

import game.Game;
import game.util.Resource;

import java.io.File;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;

public class SettingsWriter
{
	public static void writeSettings()
	{
		File file = Game.getResource().getFile(Settings.SETTINGS_PATH);
		if (!file.exists()) Game.getResource().create(file);

		write(Settings.getSettings(), file);
	}

	private static void write(Setting[] settings, File file)
	{
		try
		{
			PrintWriter writer = new PrintWriter(file, StandardCharsets.UTF_8);
			writer.print(""); //Clear the file

			for (Setting setting : settings)
			{
				writer.println(setting.toString());
			}

			writer.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
