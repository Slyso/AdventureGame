package game.settings;

public abstract class Setting<T>
{
	private String name;
	protected T value;

	protected Setting(String name, T value)
	{
		this.name = name;
		this.value = value;
	}

	@Override
	public String toString()
	{
		return name + ": " + value.toString();
	}

	public void setValue(T value)
	{
		this.value = value;
	}

	public T getValue()
	{
		return value;
	}

	public String getName()
	{
		return name;
	}
}
