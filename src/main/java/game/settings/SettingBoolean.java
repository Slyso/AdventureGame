package game.settings;

public class SettingBoolean extends Setting<Boolean>
{
	public SettingBoolean(String name, boolean standardValue)
	{
		super(name, standardValue);
	}

	public void toggleValue()
	{
		setValue(!value);
	}
}
