/*******************************************************************************
 * Copyright (C) 2018 Thomas Zahner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package game.settings;

import java.util.Arrays;

public class Settings
{
	public static final String SETTINGS_PATH = "/persistence/settings.txt";

	//Video settings
	public static SettingBoolean maxGraphicsQuality, fullscreen;
	public static SettingRange bufferStrategy;

	//Audio settings
	public static SettingBoolean music;

	//Advanced
	public static SettingBoolean debugMode;


	public static void initDefaultValues()
	{
		//Video settings
		maxGraphicsQuality = new SettingBoolean("Maximum graphics quality", true);
		fullscreen = new SettingBoolean("Fullscreen", false);
		bufferStrategy = new SettingRange("Buffer strategy", 1, 3, 2);

		//Audio settings
		music = new SettingBoolean("Music", false);

		//Advanced
		debugMode = new SettingBoolean("Debug mode", true);
	}

	public static Setting[] getSettings()
	{
		return new Setting[]{maxGraphicsQuality, fullscreen, bufferStrategy, music, debugMode};
	}

	public static Setting getSettingBy(String name)
	{
		return Arrays.stream(getSettings()).filter(setting -> setting.getName().equals(name)).findFirst().orElse(null);
	}
}
