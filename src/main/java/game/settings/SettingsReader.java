package game.settings;

import game.Game;
import game.util.FileReader;
import game.util.Resource;

import java.io.File;
import java.util.Arrays;
import java.util.Map;

public class SettingsReader
{
	public static void readSettings()
	{
		Settings.initDefaultValues();
		File file = Game.getResource().getFile(Settings.SETTINGS_PATH);

		if (!file.exists())
		{
			SettingsWriter.writeSettings();
			return;
		}

		FileReader fileReader = new FileReader(file);

		for (Map.Entry<String, String> entry : fileReader.getContent().entrySet())
		{
			parseLine(entry.getKey(), entry.getValue());
		}
	}

	private static void parseLine(String key, String value)
	{
		Setting setting = Settings.getSettingBy(key);

		if (setting instanceof SettingBoolean) applyBoolean(value, (SettingBoolean) setting);
		else if (setting instanceof SettingRange) applyInteger(value, (SettingRange) setting);
	}

	private static void applyBoolean(String value, SettingBoolean setting)
	{
		setting.setValue(value.equals("true"));
	}

	private static void applyInteger(String value, SettingRange setting)
	{
		setting.setValue(Integer.parseInt(value));
	}
}
