<h1>AdventureGame</h1>

This is the repository of a 2D Java Indie Game. <br>
You can download the game / the jar file via website: https://slyso.gitlab.io/website/

<h2>Description</h2>

<h4>Start with only one basic ability</h4>

![basic ability](https://gitlab.com/Slyso/AdventureGame/raw/master/src/main/resources/screenshots/basic%20ability.png)

<h4>Survive against diffrent monsters, doge their abilities and kill them</h4>

![adventuregame fight](https://gitlab.com/Slyso/AdventureGame/raw/master/src/main/resources/screenshots/adventuregame%20fight.png)

<h4>Collect the ablilities and coins the monsters drop and get stronger</h4>

![skills](https://gitlab.com/Slyso/AdventureGame/raw/master/src/main/resources/screenshots/skills.png)

<h4>The title screen</h4>

![title screen](https://gitlab.com/Slyso/AdventureGame/raw/master/src/main/resources/screenshots/title%20screen.png)

<h4>Some random fight scene</h4>

![fight scene](https://gitlab.com/Slyso/AdventureGame/raw/master/src/main/resources/screenshots/fight%20scene.png)


<h2>Intrested?</h2>

Download the game via homepage: https://slyso.gitlab.io/website/ <br>
Feel free to ask any questions or make any sort of contribution! <br> <br>

<h2>Copyright</h2>

<b>Copyright (C) 2018 Thomas Zahner</b>
 
This program is free software: you can redistribute it and/or modify
it under the terms of the <b>GNU General Public License</b> as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
